import pytest
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType

from etl import group_by_product


@pytest.fixture
def spark():
    return SparkSession.builder.master("local[*]").appName("test_app").getOrCreate()


def test_group_by_product_with_empty_dataframe(spark):
    # Arrange
    input_schema = StructType([])
    input_data = []
    input_df = spark.createDataFrame(data=input_data, schema=input_schema)

    # Act
    transformed_df = group_by_product(input_df)

    # Assert
    assert not transformed_df.schema
    assert transformed_df.count() == 0


def test_group_by_product(spark):
    # Arrange :
    # DataFrame representing our pharmaceutical sells data
    input_schema = StructType(
        [
            StructField("SellID", IntegerType(), True),
            StructField("Name", StringType(), True),
            StructField("Date", StringType(), True),
            StructField("Quantity", IntegerType(), True),
        ]
    )

    input_data = [
        (1, "Paracetamol", "2022-01-28", 5),
        (2, "Doliprane", "2022-01-28", 7),
        (3, "Paracetamol", "2022-01-28", 7),
        (4, "Doliprane", "2022-02-01", 9),
        (5, "Doliprane", "2022-02-01", 1),
        (6, "Paracetamol", "2022-02-02", 3),
        (7, "Probiolog", "2022-02-03", 3),
    ]
    input_df = spark.createDataFrame(data=input_data, schema=input_schema)

    # Act:
    # Apply grouping function(transformation)
    transformed_df = group_by_product(input_df)

    # Assert:
    # Assert the output of the transformation is the same as the expected dataframe

    # Expected DataFrame output of the grouping transformation
    expected_schema = StructType(
        [
            StructField("Name", StringType(), True),
            StructField("TotalQuantity", LongType(), True),
        ]
    )

    expected_data = [
        ("Doliprane", 17),
        ("Paracetamol", 15),
        ("Probiolog", 3),
    ]
    expected_df = spark.createDataFrame(data=expected_data, schema=expected_schema)

    field_list = lambda fields: (fields.name, fields.dataType, fields.nullable)
    transformed_fields = [*map(field_list, transformed_df.schema.fields)]
    expected_fields = [*map(field_list, expected_df.schema.fields)]

    assert set(transformed_fields) == set(expected_fields)  # schema comparison
    assert sorted(transformed_df.collect()) == sorted(
        expected_df.collect()
    )  # data comparison
