# Tester avec PySpark

Tester, particulièrement en TDD, est crucial pour prévenir les régressions dans le code et également pour s'assurer que le code fait ce pour quoi il a été conçu. C'est tout aussi vrai pour le Data Engineering que pour le développement logiciel pur.

Dans ce repo, vous trouverez un mini exemple de projet de Data Engineering réalisé en TDD
Il s'agit du projet fil rouge associé à la série d'articles **Craftmanship et Data Engineering** du [blog d'Arolla](https://www.arolla.fr/blog/)

## Description du projet
Dans ce projet, nous allons supposer que nous sommes Data Engineering pour une pharmacie et disposons de données répertoriant les différentes ventes effectuées dans cette pharmacie ainsi que des informations associées à ces ventes (désignation du produit pharmaceutique, date de vente, quantité). Notre mission est de construire un ETL à l'aide de PySpark dont le rôle est de regrouper les ventes par désignation afin de déterminer les quantités vendues pour chaque produit pharmaceutique.

Le code simplifié de l'ETL se trouve dans le fichier **etl.py** et les tests associés sont dans **test_etl.py**.

La librairie de test utilisé dans ce projet est [pytest](https://pypi.org/project/pytest/) que vous pouvez installer à l'aide de la commande `pip install pytest`