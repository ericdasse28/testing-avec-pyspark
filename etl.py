from pyspark.sql.functions import sum


def group_by_product(df):
    if df.count() == 0:
        return df
    grouped_df = df.groupBy(
        "Name",
    ).agg(sum("Quantity").alias("TotalQuantity"))
    return grouped_df
